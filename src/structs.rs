#[derive(Debug)]
pub struct TargetData {
    pub sens_res: Vec<u8>,
    pub sel_res: u8,
    pub nfcid: Vec<u8>,
    pub ats: Vec<u8>
}
