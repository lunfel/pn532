use crate::structs::TargetData;

#[derive(Debug)]
pub struct Response {
    pub cmd_response_code: u8,
    pub data: Option<InListPassiveTarget>
}

impl Response {
    pub fn new(data: &[u8]) -> Self {
        Response {
            cmd_response_code: data[0],
            data: match data[0] {
                0x4B => Some(InListPassiveTarget::from_data(&data[1..])),
                _ => None
            }
        }
    }
}

trait CommandResponse {
    fn from_data(data: &[u8]) -> Self;
}

#[derive(Debug)]
pub struct InListPassiveTarget {
    pub targets: Vec<TargetData>
}

impl CommandResponse for InListPassiveTarget {
    fn from_data(data: &[u8]) -> Self {
        let nb_targets = data[0];
        let mut targets: Vec<TargetData> = vec![];

        let mut target_start = 1;
        for _target_index in 0..nb_targets {
            let nfcid_len = data[target_start + 4] as usize;
            let ats_len_index = target_start + 5 + nfcid_len;
            let ats_len = if ats_len_index >= data.len() {
                0
            } else {
                data[target_start + 5 + nfcid_len] as usize
            };

            targets.push(TargetData {
                // note that the spec is confusingly missing a one byte enumerator prefix
                sens_res: data[target_start + 1..target_start + 2].to_vec(),
                sel_res: data[target_start + 3],
                nfcid: data[target_start + 5..target_start + 5 + nfcid_len].to_vec(),
                ats: if ats_len == 2 {
                    target_start = target_start + 5 + nfcid_len + 1;

                    vec![]
                } else if ats_len == 0 {
                    vec![]
                } else {
                    let ats_start = target_start + 5 + nfcid_len + 1;

                    let ats = data[ats_start..ats_start + ats_len].to_vec();

                    target_start = ats_start + ats_len;

                    ats
                }
            });
        }

        InListPassiveTarget {
            targets
        }
    }
}
