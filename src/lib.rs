extern crate i2cdev;

mod commands;
pub mod responses;
mod structs;

use i2cdev::core::I2CDevice;
use i2cdev::linux::LinuxI2CDevice;
use i2cdev::linux::LinuxI2CError;
use std::time::{Duration, Instant};
use std::thread::sleep;
use crate::commands::Command;
use crate::responses::Response;

const PN532_ADDR:           u8 = 0x24;

// commands


#[allow(unused)]
pub enum CardType {
    IsoTypeA  = 0x00,
    FeliCa212 = 0x01,
    FeliCa424 = 0x02,
    IsoTypeB  = 0x03,
    Jewel     = 0x04,
}

// Not the same thing than IsoTypeA/B
pub enum MifareKeyTypes {
    MifareAuthA = 0x60,
    MifareAuthB = 0x61
}

pub enum MifareCommands {
    MifareCmdRead = 0x30,
    MifareCmdWrite = 0xA0,
    MifareCmdTransfer = 0xB0,
    MifareCmdDecrement = 0xC0,
    MifareCmdIncrement = 0xC1,
    MifareCmdStore = 0xC2
}

pub enum TgInitAsTargetMode {
    PassiveOnly = 1 << 0,
    DepOnly = 1 << 1,
    PiccOnly = 1 << 2,
}

pub struct Pn532 {
    i2c: LinuxI2CDevice,
}

pub type I2CResult<R> = Result<R, LinuxI2CError>;

impl Pn532 {
    pub fn open(dev: &str) -> I2CResult<Self> {
        let i2c = LinuxI2CDevice::new(dev, PN532_ADDR.into())?;
        Ok(Self {
            i2c
        })
    }

    // information frame, see UM0701-02 page 28
    fn send_frame(&mut self, payload: &[u8]) -> I2CResult<()> {
        assert!(payload.len() < 0xfe);

        let len = payload.len() as u8 + 1;
        let len_checksum = 0u8.wrapping_sub(len);

        // calculating checksum
        let mut checksum = 0u8.wrapping_sub(0xd4);
        for p in payload {
            checksum = checksum.wrapping_sub(*p);
        }

        let mut b = vec![
            0x00, // sync preamble
            0x00, 0xff,  // start
        ];
        b.push(len);
        b.push(len_checksum);
        b.push(0xd4); // direction
        b.extend_from_slice(payload);
        b.push(checksum);
        b.push(0x00); // postamble

        self.i2c.write(&b)
    }

    fn expect_ack(&mut self) -> I2CResult<()> {
        for _ in 0..3{
            sleep(Duration::from_millis(1));
            let mut b = [0u8; 256];
            self.i2c.read(&mut b)?;

            let mut state = 0;
            for i in 0..b.len() {
                match (b[i], state) {
                    (0x00, 0) => {
                        state = 1;
                    }
                    (0x00, 1) => {
                        state = 1;
                    }
                    (0xff, 1) => {
                        state = 2;
                    }
                    (0x00, 2) => {
                        return Ok(());
                    }
                    (0xff, 2) => {
                        return Err(std::io::Error::new(std::io::ErrorKind::Other, "nack").into());
                    }
                    (1 , 2) => {
                        return Err(std::io::Error::new(std::io::ErrorKind::Other, format!("ack error 0x{:x}", b[i+2])).into())
                    },
                    (_, 2) => {
                        return Err(std::io::Error::new(std::io::ErrorKind::Other, "out of order").into());
                    },
                    _ => {
                        state = 0;
                    }
                }
            }
        }
        Err(std::io::Error::new(std::io::ErrorKind::TimedOut, "timeout").into())
    }

    fn receive_frame(&mut self, timeout: Duration) -> I2CResult<Vec<u8>> {
        let now = Instant::now();
        loop {
            if now.elapsed() > timeout {
                return Err(std::io::Error::new(std::io::ErrorKind::TimedOut, "timeout").into());
            }
            sleep(Duration::from_millis(1));
            let mut b = [0u8; 256];
            self.i2c.read(&mut b)?;

            let mut state = 0;
            for i in 0..b.len() {
                match (b[i], state) {
                    (0x00, 0) => {
                        state = 1;
                    }
                    (0x00, 1) => {
                        state = 1;
                    }
                    (0xff, 1) => {
                        state = 2;
                    }
                    (0x00, 2) => {
                        // ack frame
                        break;
                    }
                    (0xff, 2) => {
                        // nack frame or extended
                        break;
                    }
                    (0x01 , 2) => {
                        return Err(std::io::Error::new(std::io::ErrorKind::Other, format!("receive error 0x{:x}", b[i+2])).into())
                    },
                    (size, 2) => {
                        let res = b[i + 3..i + 3 + (size as usize - 1)].to_vec();

                        return Ok(res);
                    },
                    _ => {
                        state = 0;
                    }
                }
            }
        }
    }

    // ( IC version , firmware version, firmware revision, feature bitfield)
    pub fn get_firmware_version(&mut self) -> I2CResult<(u8,u8,u8,u8)> {
        self.send_frame(&[Command::GetFirmwareVersion as u8])?;
        self.expect_ack()?;
        let r = self.receive_frame(Duration::from_millis(10))?;
        Ok((r[1],r[2],r[3],r[4]))
    }

    pub fn powerdown(&mut self) -> I2CResult<()> {
        self.send_frame(&[
            Command::PowerDown as u8,
            0b10000011,
        ])?;
        self.expect_ack()?;

        // according to page 98 remarks, we need to lock the bus for 1ms,
        // otherwise the chip might get confused
        sleep(Duration::from_millis(1));

        Ok(())
    }

    pub fn setup(&mut self) -> I2CResult<()> {
        self.send_frame(&[
            Command::SAMConfiguration as u8,
            0x01 // normal mode
        ])?;
        self.expect_ack()?;
        Ok(())
    }

    pub fn list2(&mut self, timeout: Duration) -> I2CResult<Response> {
        self.send_frame(&[
            Command::InListPassiveTarget as u8,
            0x02, // max-targets. the chip only supposed 2, so i dunno why this is a parameter
            CardType::IsoTypeA as u8,
        ])?;
        self.expect_ack()?;

        let r = self.receive_frame(timeout)?;

        Ok(Response::new(&r))
    }

    pub fn list(&mut self, timeout: Duration) -> I2CResult<Vec<Vec<u8>>> {
        self.send_frame(&[
            Command::InListPassiveTarget as u8,
            0x02, // max-targets. the chip only supposed 2, so i dunno why this is a parameter
            CardType::IsoTypeA as u8,
        ])?;
        self.expect_ack()?;

        let r = self.receive_frame(timeout)?;

        if r.len() < 5 {
            return Ok(Vec::new());
        }

        let num = r[1];
        let mut i = 2;

        let mut tags = Vec::new();
        for _ in 0..num {
            if i >= r.len() {
                return Ok(Vec::new());
            }
            i   += 1 // note that the spec is confusingly missing a one byte enumerator prefix
                +  2 // sens_res
                +  1 // sel_res
            ;

            if i >= r.len() {
                return Ok(Vec::new());
            }
            let len     = r[i] as  usize;
            i += 1;
            if i >= r.len() {
                return Ok(Vec::new());
            }
            if i + len  > r.len() {
                return Ok(Vec::new());
            }
            tags.push(r[i .. i + len].to_vec());
            i += len;

            //ats
            if i < r.len() {
                let len = r[i] as  usize;
                // the chip doesn't tell us if there's an ats field, it just emits one or not.
                // in this case the the ats length field will be the index 2, which is also not a valid ats size,
                if len == 2 {
                    continue;
                }
                i += len;
            }
        }

        Ok(tags)
    }

    pub fn get_general_status(&mut self, timeout: Duration) -> I2CResult<Vec<u8>> {
        self.send_frame(&[Command::GetGeneralStatus as u8])?;

        self.expect_ack()?;

        let r = self.receive_frame(timeout)?;

        Ok(r)
    }

    pub fn in_atr(&mut self, tg: u8, timeout: Duration) -> I2CResult<Vec<u8>> {
        self.send_frame(&[
            Command::InATR as u8,
            tg,
            0x00
        ])?;

        self.expect_ack()?;

        let r = self.receive_frame(timeout)?;

        Ok(r)
    }

    pub fn mifare_classic_authenticate_block(&mut self, uid: &[u8], block_number: u8, key_type: MifareKeyTypes, key: &[u8]) -> I2CResult<Vec<u8>> {
        let mut payload: Vec<u8> = vec![];

        payload.push(Command::InDataExchange as u8);
        payload.push(0x01); // Max card numbers
        payload.push(key_type as u8);
        payload.push(block_number);

        payload.extend_from_slice(key);
        payload.extend_from_slice(uid);

        self.send_frame(&payload)?;

        let r = self.receive_frame(Duration::from_secs(1))?;

        if r.len() != 2 {
            return Err(std::io::Error::new(std::io::ErrorKind::Other, "invalid response length").into());
        }

        match r[..] {
            [0x41, 0x00] => Ok(r),
            [0x41, 0x14] => Err(std::io::Error::new(std::io::ErrorKind::PermissionDenied, "authentication error").into()),
            _ => Err(std::io::Error::new(std::io::ErrorKind::Other, "other error in authenticate sector").into())
        }
    }

    pub fn mifare_classic_read_block(&mut self, block_number: u8) -> I2CResult<Vec<u8>> {
        self.send_frame(&[
            Command::InDataExchange as u8,
            0x01,
            MifareCommands::MifareCmdRead as u8,
            block_number
        ])?;

        let r = self.receive_frame(Duration::from_secs(1))?;

        Ok(r)
    }
}



